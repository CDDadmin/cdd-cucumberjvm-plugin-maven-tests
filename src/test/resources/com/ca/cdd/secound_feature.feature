Feature: Is it Friday yet 1?
  Everybody wants to know when it's Friday

  Scenario: Sunday
    Given today is "Sunday"
    When I ask whether it's Friday yet
    Then I should be told "Nope"


  Scenario: Friday
    Given today is "Friday"
    When I ask whether it's Friday yet
    Then I should be told "TGIF"


  Scenario: Tuesday
    Given today is "Monday"
    When I ask whether it's Friday yet
    Then I should be told "Nope"

  Scenario: Wednesday
    Given today is "Monday"
    When I ask whether it's Friday yet
    Then I should be told "Nope"


